import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, Inject } from '@angular/core';
import { Animal } from './animals.interface';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-animals',
  templateUrl: './animals.component.html',
  styleUrls: ['./animals.component.css']
})
export class AnimalsComponent {
  public animals: Animal[];
  public animal: Animal;
  public showForm = false;
  constructor(public http: HttpClient, @Inject('BASE_URL') public baseUrl: string) {
    this.refresh();
   }

  
  newAnimal(){
    this.showForm = true;
    this.animal = {
      id: -1,
      name: '',
      weight: 0,
      habitat: '',
      isWild: true,
      dateOfBirth: new Date()
    };
  }

  refresh() {
    this.http.get<Animal[]>(this.baseUrl + 'api/animals').subscribe(result => {
      this.animals = result;
      this.showForm = false;
    }, error => console.error(error));
  }

  edit(animal: Animal){
    this.animal = animal;
    this.showForm = true;
  }

  deleteAnimal(animal: Animal){
    this.http.delete(this.baseUrl + 'api/animals/' + animal.id).subscribe(result => {
      this.refresh();
    }, error => console.error(error));
  }

  save() {
    if(this.animal.id > 0){
      //update
      this.http.put<Animal>(this.baseUrl + 'api/animals/' + this.animal.id, this.animal).subscribe(result => {
        this.refresh();
      }, error => console.error(error));
      return;
    }
    //insert
    this.http.post<Animal>(this.baseUrl + 'api/animals', {
      name: this.animal.name,
      weight:  this.animal.weight,
      habitat:  this.animal.habitat,
      isWild:  this.animal.isWild,
      dateOfBirth:  this.animal.dateOfBirth,
    }).subscribe(result => {
      this.refresh();
    }, error => console.error(error));
  }

}
