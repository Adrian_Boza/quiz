export interface Animal {
    id: number;
    name: string;
    weight: number;
    isWild: boolean;
    habitat: string;
    dateOfBirth: Date;
}
  