using System;
namespace Animals.Models
{

    public class Employee{

            public int Id {get; set; }
            public string Name {get; set; }
            public bool Manager {get; set; }
            public string Address {get; set; }
            public DateTime DateOfBirth { get; set; }
            
    }
}