using System;
namespace Animals.Models
{

    public class Animal{

            public int Id {get; set; }
            public string Name {get; set; }
            public double Weight {get; set; }
            public bool isWild {get; set; }
            public string habitat {get; set; }
            public DateTime DateOfBirth { get; set; }
            
    }
}