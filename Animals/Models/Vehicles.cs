using System;
namespace Animals.Models
{

    public class Vehicle{

            public int Id {get; set; }
            public string Name {get; set; }
            
            public bool Gasoline {get; set; }
            public string VehicleBrand {get; set; }
            public string Model { get; set; }
            
    }
}