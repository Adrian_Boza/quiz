using Microsoft.EntityFrameworkCore;

namespace Animals.Models
{
    public class DataBaseContext : DbContext
    {
        public DataBaseContext(DbContextOptions<DataBaseContext> options) : base(options)
        {
            
        }
        public DbSet<Animal> Animals { get; set; }
        public DbSet<Deparment> Deparments { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }
        
    }
}