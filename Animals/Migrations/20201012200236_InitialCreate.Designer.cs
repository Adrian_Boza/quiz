﻿// <auto-generated />
using Animals.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Animals.Migrations
{
    [DbContext(typeof(DataBaseContext))]
    [Migration("20201012200236_InitialCreate")]
    partial class InitialCreate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn)
                .HasAnnotation("ProductVersion", "3.1.8")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            modelBuilder.Entity("Animals.Models.Animal", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id")
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("Name")
                        .HasColumnName("name")
                        .HasColumnType("text");

                    b.Property<double>("Weight")
                        .HasColumnName("weight")
                        .HasColumnType("double precision");

                    b.Property<string>("habitat")
                        .HasColumnName("habitat")
                        .HasColumnType("text");

                    b.Property<bool>("isWild")
                        .HasColumnName("is_wild")
                        .HasColumnType("boolean");

                    b.HasKey("Id")
                        .HasName("pk_animals");

                    b.ToTable("animals");
                });
#pragma warning restore 612, 618
        }
    }
}
