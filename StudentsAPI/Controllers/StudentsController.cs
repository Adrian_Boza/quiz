using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using StudentsAPI.Models;

namespace StudentsAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class StudentsController : ControllerBase
    {
       
        
        private List<Student> students;
        public StudentsController(ILogger<StudentsController> logger)
        {
            this.students = new List<Student>();
            var Pedro = new Student();
                Pedro.Id = 1;
                Pedro.Name = "Pedro";
                Pedro.LastName = "Perez";
                Pedro.Age = 15;
                Pedro.telephone = 61019810;
                
            var Adrian = new Student();
                Adrian.Id = 2;
                Adrian.Name = "Adrian";
                Adrian.LastName = "Boza";
                Adrian.Age = 21;
                Adrian.telephone = 86301379;


            this.students.Add(Pedro);
            this.students.Add(Adrian);


        }

        /* Get /api/students -> trae el arreglo de todos los estudiantes*/
        [HttpGet]
        public IEnumerable<Student> GetAll()
        {
            return this.students;
        }
         /* Get /api/students/1 -> Retorna un estudiante en especifico*/
        [HttpGet("{id}")]
        public ActionResult<Student> Get(int id)
        {
            var student = this.students.Find(f => f.Id == id);
            if(student == null){
                return NotFound();
            }else{
                return student;
            }
        }

         /* Post /api/students -> Agregar un estudiante al arreglo*/
        [HttpPost]
        public ActionResult<Student> Post(Student student)
        { 
          var id = this.students.Max(f => f.Id);
          student.Id = id+1;
          this.students.Add(student);

          return student;
        }
        /* DELETE /api/students/1 -> Elimina un estudiante especifico del arreglo*/
        [HttpDelete("{id}")]
        public ActionResult<Student> Delete(int id)
        {
            var student = this.students.Find(f => f.Id == id);
            if(student == null){
                return NotFound();
            }else{
                this.students.Remove(student);
                return student;
            }
        }
        /* PACTH /api/students/1 -> Edita un estudiante especifico del arreglo*/
        [HttpPatch("{id}")]
        public ActionResult<Student> Patch(int id, Student student)
        {
            var dbStudent = this.students.Find(f => f.Id == id);
            if(dbStudent == null){
                return NotFound();
            }else{

                dbStudent.Name = student.Name;
                dbStudent.LastName = student.LastName;
                dbStudent.Age = student.Age;
                dbStudent.telephone = student.telephone;
                
                return student;
            }
        }
    }
}
