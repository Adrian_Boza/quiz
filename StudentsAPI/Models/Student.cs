namespace StudentsAPI.Models
{

    public class Student{

            public int Id {get; set; }
            public string LastName {get; set; }
            public string Name {get; set; }
            public int Age {get; set; }
            public int telephone {get; set; }
            
    }
}